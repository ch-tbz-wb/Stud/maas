Installation MAAS
-----------------

[[_TOC_]]

### Master
 
[Ubuntu](https://ubuntu.com/download/desktop) als Desktop aufsetzen. Dabei genügt die minimale Version. 

Fixe IP-Adresse vergeben, z.B. über Einstellungen, Software Update durchführen und MAAS Installieren

    sudo apt-add-repository ppa:maas/3.5
    sudo apt update
    sudo apt upgrade -y
    sudo apt install -y maas jq markdown nmap traceroute git curl wget zfsutils-linux cloud-image-utils virtinst qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils whois 

MAAS Admin User erstellen. Als Username `ubuntu` verwenden. 

    sudo maas createadmin 
    
MAAS Admin User Name `ubuntu` als Umgebungvariable setzen. Erlaubt einen einfacheren Zugriff via CLI.

    cat <<%EOF% >>$HOME/.bashrc
    export PROFILE=ubuntu
    %EOF%
    
SSH-Key erstellen, den brauchen wir nachher

    ssh-keygen    
    
Browser starten und UI von MAAS aufrufen [http://localhost:5240](http://localhost:5240)

* SSH-Key, von vorher `cat ~/.ssh/id_rsa.pub`  eintragen
* Den MAAS Master (braucht es für die interne Namensauflösung) und die bekannten DNS Server eintragen
* Bei Subnets DHCP Server aktivieren auf z.B. 10.0.31.x, Gateway IP: 10.0.31.1 und DNS Server von OpenDNS 208.67.222.222, 208.67.220.220 eintragen.
* **Beim Router ist DHCP zu deaktivieren!**

### Worker Nodes   

Die Worker Nodes sind so zu Konfigurieren, dass sie via Netzwerk (PXE Boot) booten.

Anschliessend sind die zwei Installationsroutinen durchzuführen. 

- - -

[![](https://img.youtube.com/vi/jj1M-YyCgD4/0.jpg)](https://www.youtube.com/watch?v=jj1M-YyCgD4)

MAAS Enlistment (auf Bild klicken, damit YouTube Film startet)

---

[![](https://img.youtube.com/vi/k-9VHZg_qoo/0.jpg)](https://www.youtube.com/watch?v=k-9VHZg_qoo)

MAAS Commission (auf Bild klicken, damit YouTube Film startet)

- - -

Die neue Maschine anklicken und rechts oben mittels `Take action` -> Deploy die Software deployen (Ubuntu 20.04). Um auf der Maschine nachher virtuelle Maschinen erstellen zu können ist die Checkbox `Register as MAAS KVM host` zu aktivieren und `libvirt` auszuwählen.

Nach der Installation steht die Maschine unter `KVM` -> `Virsh`zur Verfügung und es lassen sich neue virtuelle Maschinen darauf erstellen (Compose).

**Tips** 
* Normale PCs haben keine Unterstützung für [BMC](https://de.wikipedia.org/wiki/Baseboard_Management_Controller) deshalb muss der `Power type` auf `Manuel` eingestellt werden. 
* **Optional**: TBZ PCs haben integriertes Intel AMT. Power On, `Ctrl-P`, Default Password `admin` durch internes Ersetzen, IP Einstellungen vornehmen (ich verwende fixe IP ist mehr Aufwand aber nachher einfacher und **TCP Aktivieren**. Probieren mit `http://IP-Adresse:16992`. Vorsicht das AMT UI verwendet fix den US Tastaturlayout.
* PCs mittels `Lock` vor unbeabsichtigtem Ändern schützen.
* Nachdem die Maschine unter `KVM` -> `Virsh` sichtbar ist, diese anklicken und in `Settings` `CPU overcommit` auf 10 setzen. Dann können mehr VMs erstellt werden. Der Memory Wert sollte nur erhöht werden, wenn nicht benötigte VMs ausgeschaltet werden und so wieder Memory zu Verfügung steht.

### MAAS CLI und Tests

Einlogen für CLI Access (die Umgebungsvariable haben wir oben gesetzt!)

    maas login ${PROFILE} http://localhost:5240/MAAS/api/2.0
    
Der verlangte API Key finden wir im UI von MAAS unter `ubuntu` -> `API-Keys`.    
    
Mögliche Befehle anzeigen

    maas ${PROFILE} --help
    
Erstelle Maschinen im JSON Format ausgeben:

    maas ${PROFILE} machines read
    
Es sollten die KVM Maschinen angezeigt werden.    

**VMs erstellen**

Dazu ist zuerst die IDs der KVM Maschinen (alt Pod) holen:

    maas ${PROFILE} pods read | jq '.[] | { name: .name, id: .id}'
    
Eine ID aussuchen und VM erstellen

    maas ${PROFILE} pod compose ${pod} hostname=test
    
Die `system_id` notieren, die brauchen wir für die Bereitstellung (Deploy) der Software auf der VM.

    maas ${PROFILE} machine deploy ${system_id}
    
Steht ein Cloud-init Script zur Verfügung, kann dieses im Base64 Format (auf einer Zeile) mitgegeben werden.      

    maas ${PROFILE} machine deploy ${system_id} data=$(base64 -w0 cloud-init.yaml)
    
Wird die VM nicht mehr gebraucht, zuerst `release` dann `delete` ausführen.

    maas ${PROFILE} machine release ${system_id}
    maas ${PROFILE} machine delete ${system_id}
    
**Tipp**

Pakete über das Internet zu laden, kann relativ grossen Netzwerkverkehr verursachen. Deshalb sollten Paketquellen aus der Schweiz verwendet werden, z.B. `https://mirror.init7.net/ubuntu/`.

Die Paketquellen können in der MAAS Oberfläche unter `Settings` -> `Packages repos` angepasst werden. Dabei ist der erste Eintrag mit `http://archive.ubuntu.com/ubuntu` zu ersetzen. Bei neuen Deployen der VMs werden dann diese verwendet.
  
## Links

* [MAAS Blog](https://maas.io/blog)
