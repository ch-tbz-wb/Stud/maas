## Private Cloud oder Metal as a Service (MAAS)

[[_TOC_]]

![](https://github.com/mc-b/lernmaas/raw/master/doc/images/howitworks.png)

Quelle: maas.io

- - - 

### MAAS

Canonical (Ubuntu) MAAS bittet schnelle Serverbereitstellung für Ihr Rechenzentrum.

Dabei MAAS verfügt über eine abgestufte Architektur
* Ein 'Region Controller (regiond)' nimmt die Bereitstellungsanforderungen via UI oder REST-API entgegen.
* Rack Controller (Rackd) verwalten die Bare-Metal Server wo die virtuellen Maschinen betrieben werden.

**Zusammengefasst**: [MAAS](https://maas.io/) verwandelt Ihr Rechenzentrum in eine Bare-Metal-Cloud.

### VPN

*Das konventionelle VPN bezeichnet ein virtuelles privates (in sich geschlossenes) Kommunikationsnetz. Virtuell in dem Sinne, dass es sich nicht um eine eigene physische Verbindung handelt, sondern um ein bestehendes Kommunikationsnetz, das als Transportmedium verwendet wird. Das VPN dient dazu, Teilnehmer des bestehenden Kommunikationsnetzes an ein anderes Netz zu binden.*

Um auf das MAAS Netzwerk zugreifen zu können, verwenden wir ein VPN.

**Links**

* [VPN](https://de.wikipedia.org/wiki/Virtual_Private_Network)
* [OpenVPN](https://openvpn.net/)

### Hands-on

* [Installation MAAS](maas-install.md)
* [Gemeinsame Datenablage](https://github.com/mc-b/lernmaas/blob/master/doc/MAAS/Install.md#gemeinsame-datenablage-optional) **(optional)** 

Eines der beiden VPNs installieren

* [Installation OpenVPN](openvpn-install.md) 
* [Installation WireGuard](wireguard-install.md)

### Selbststudium

* [Immutable Infrastructure](https://www.hashicorp.com/resources/what-is-mutable-vs-immutable-infrastructure)

**Produkte**

* [MAAS.io](https://maas.io)
* [OpenVPN](https://openvpn.net/)
* [WireGuard](https://www.wireguard.com/)

**Projekte mit dem Ziel verschiedene Services auf unterschiedlichen Plattformen/Cloud zur Verfügung zu stellen.**

* [MAAS.io Plattform](https://github.com/mc-b/lernmaas)
* [Multi-Cloud](https://github.com/mc-b/lerncloud)
* [GNS3 (Digitaler Zwilling](https://github.com/mc-b/lerngns3)



