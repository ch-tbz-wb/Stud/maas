## WireGuard - VPN für VMs in der (Private) Cloud 

[[_TOC_]]

WireGuard ist eine freie Software zum Aufbau eines virtuellen privaten Netzwerkes über eine verschlüsselte Verbindung. Als Besonderheit ist diese VPN-Technik direkt im Linux-Kernel ab Version 5.6 integriert und erlaubt so eine höhere Verarbeitungsgeschwindigkeit als vergleichbare Lösungen wie IPsec oder OpenVPN.

### Variante a) Cloud-init

Dabei werden die VPN Information für WireGuard im Cloud-init Script mitgegeben.

Das verletzt jedoch, Regel 3 von [The Twelve-Factor App](https://12factor.net/), deshalb als dauerhafte Lösung nicht Empfehlenswert.


    #cloud-config
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        shell: /bin/bash
        ssh_import_id:
         - gh:mc-b
        ssh_authorized_keys:
          - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPvLEdsh/Vpu22zN3M/lmLE8zEO1alk/aWzIbZVwXJYa1RbNHocyZlvE8XDcv1WqeuVqoQ2DPflkQxdrbp2G08AWYgPNiQrMDkZBHG4GlU2Jhe9kCRiWVx/oVDeK8v3+w2nhFt8Jk/eeQ1+E19JlFak1iYveCpHqa68W3NIWj5b10I9VVPmMJVJ4KbpEpuWNuKH0p0YsUKfTQdvrn42fz5jYS1aV7qCCOOzB3WC833QRy04iHZObxDWIi/IFeIp1Gw2FkzPhoZyx4Fk9bsXfm301IePp9cwzArI2LdcOhwEZ3RW2F7ie2WJlVy5tzJjMGCaE1tZTjiCahLNEeTiTQp public-key@cloud.tbz.ch   
    packages:
      - wireguard
    write_files:
     - content: |
        [Interface]
        Address = 10.9.39.20/24
        PrivateKey = ...
        [Peer]
        PublicKey = ...
        Endpoint = 10.0.x.y:51200
        AllowedIPs = 10.9.39.0/24
        PersistentKeepalive = 25
       path: /etc/wireguard/wg0.conf
       permissions: '0640'
    runcmd:
      - sudo systemctl enable wg-quick@wg0.service
      - sudo systemctl start wg-quick@wg0.service

### Variante b) Metadata

Jede Cloud stellt, sogenannte Metadata zur Verfügung.

Mittels diesen Metadata können Informationen wie 
* Cloud Region
* Computer Name
* Betriebssystem etc.
ausgelesen werden.

Diese Informationen sind nützlich um, z.B.
* VPN einzurichten
* Spezifische Software zu installieren
* Cloud-init Scripts zu Parametrisieren
* Etc.

Während Azure und AWS Zugriff auf Metadata von der Installierten VM unterstützen, führt der Weg bei [MAAS](https://maas.io) über [Curtin](https://curtin.readthedocs.io/en/latest/topics/config.html).


Dazu, ist auf dem Rack Server, im Verzeichnis `/etc/maas/preseeds/` nach folgendem Namensschema eine sogenannte Preseed Datei zu erstellen:

    {prefix}_{osystem}_{node_arch}_{node_subarch}_{release}_{node_name}
    {prefix}_{osystem}_{node_arch}_{node_subarch}_{release}
    {prefix}_{osystem}_{node_arch}_{node_subarch}
    {prefix}_{osystem}_{node_arch}
    {prefix}_{osystem}
    {prefix}

Für die Ubuntu Images ergibt sich folgender Dateiname: `/etc/maas/preseeds/curtin_userdata_ubuntu`.

Die Preseed [Datei](https://github.com/mc-b/lernmaas/blob/master/preseeds/curtin_userdata_ubuntu) von Projekt [lernMAAS](https://github.com/mc-b/lernmaas) sieht wie folgt aus:

    #cloud-config Ubuntu 18.04
    debconf_selections:
     maas: |
      {{for line in str(curtin_preseed).splitlines()}}
      {{line}}
      {{endfor}}
    #
    late_commands:
      maas: [wget, '--no-proxy', {{node_disable_pxe_url|escape.json}}, '--post-data', {{node_disable_pxe_data|escape.json}}, '-O', '/dev/null']
      10_git: ["curtin", "in-target", "--", "sh", "-c", "apt-get -y install git curl wget jq markdown nmap traceroute"]
    {{if 'cloud-init' in (node.tag_names())}}
      20_git: ["curtin", "in-target", "--", "sh", "-c", "git clone {{node.description}} /opt/lernmaas"]
      25_run: ["curtin", "in-target", "--", "sh", "-c", "cp /opt/lernmaas/cloud.cfg.d/* /etc/cloud/cloud.cfg.d/"]
    {{else}}
      30_git: ["curtin", "in-target", "--", "sh", "-c", "git clone https://github.com/mc-b/lernmaas /opt/lernmaas"]
      35_run: ["curtin", "in-target", "--", "sh", "-c", "cp /opt/lernmaas/cloud.cfg.d/* /etc/cloud/cloud.cfg.d/"]
    {{endif}}
      55_vwg: ["curtin", "in-target", "--", "sh", "-c", "/bin/echo {{node.zone.description}} >/opt/lernmaas/wireguard"]
      60_tag: ["curtin", "in-target", "--", "sh", "-c", "/bin/echo {{node.tag_names()}} >/opt/lernmaas/tags"]
      70_wig: ["curtin", "in-target", "--", "sh", "-c", "cd /opt/lernmaas && wget https://raw.githubusercontent.com/mc-b/lernmaas/master/services/wireguard.sh && bash -x ./wireguard.sh" ]

Die Befehle mit Zahlen vorangestellt werden ausgeführt:
* 10 - Installiert benötigte Packete
* 20 + 25 - werden ausgeführt wenn der Tag `cloud-init` gesetzt ist. Dann wir statt lernmaas das Repository aus dem Feld `description` geclont.
* 30 - 35 - lernmaas Repository clonen und Cloud-init Scripts für Installation abstellen
* 55 - 70 - Tags und WireGuard (aus Feld `Description` im Eintrag AZ) als Dateien speichern und WireGuard Installation durchführen.

Anschliessend ist eine oder mehrere AZs anzulegen und im Feld `Description` die WireGuard Konfigurationen im Base64 Format abzulegen.

Dateien mit WireGuard Konfigurationen werden, in der Regel, beim Aufsetzen des [MAAS Gateways](https://github.com/mc-b/lernmaas/blob/master/doc/MAAS/Gateway.md) erstellt.

**Links**

* [Customising MAAS installs](https://ubuntu.com/blog/customising-maas-installs)
* [curtin](https://maas.io/docs/custom-node-setup-preseed) 
* [Customising MAAS](https://ubuntu.com/blog/customising-maas-installs)
* [Customising MAAS installs](http://mattjarvis.org.uk/post/customising-maas/)
* [Custom partitioning with Maas and Curtin](http://caribou.kamikamamak.com/2015/06/26/custom-partitioning-with-maas-and-curtin-2/)
* [Azure Instance Metadata Service](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/instance-metadata-service?tabs=linux)
* [AWS Instance metadata and user data](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html)



      
