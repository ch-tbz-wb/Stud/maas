## OpenVPN  

[[_TOC_]]

OpenVPN ist eine freie Software zum Aufbau eines Virtuellen Privaten Netzwerkes über eine verschlüsselte TLS-Verbindung. OpenVPN verwendet wahlweise UDP oder TCP zum Transport.

### Variante a) Network Manager (Ubuntu Desktop)

Network Manager strebt eine Netzwerkkonnektivität an, die „einfach funktioniert“. Der Computer sollte die kabelgebundene Netzwerkverbindung verwenden, wenn er eingesteckt ist, aber automatisch zu einer drahtlosen Verbindung wechseln, wenn der Benutzer sie aussteckt und sich vom Schreibtisch entfernt.

NetworkManager ist standardmässig bei Ubuntu Desktop installiert.

* [Network Manager](https://help.ubuntu.com/community/NetworkManager) - nicht getestet.

### Variante b) Manuelle Installation (Ubuntu Desktop)

Dazu brauchen wir zuerst auch wieder den Network Manager um eine Bridge zu erstellen.

Eine Anleitung finden wir [hier](https://www.xmodulo.com/configure-linux-bridge-network-manager-ubuntu.html).

Kurzanleitung:
* Connection Manager starten `sudo nm-connection-editor` 
* Neue Bridge hinzufügen mit Device Name `br0`
* IPv4 manuell Konfigurieren, Werte dafür von der Wired Connection übernehmen. Fixe IP (10.x.y.8), DNS (208.67.222.222 · 208.67.220.220) etc.
* IPv6 deaktivieren (disable).
* Ethernet, zur Bridge, hinzufügen
* Alte Wired Connection löschen.
* Restart NetworkManager `sudo systemctl restart NetworkManager.service`
* Überprüfen ob wir mit dem Internet verbunden sind, z.B. mit `ping google.ch`
* Überprüfen ob `eno?` Adapter keine IP-Adresse mehr hat und `br0` dafür eine `ip addr`

Als nächstes OpenVPN Installieren, dazu das Script vom Projekt [lerngns3](https://github.com/mc-b/lerngns3) verwenden. 

Dieses ist als normaler User (ubuntu) auszuführen:

    curl -sfL https://raw.githubusercontent.com/mc-b/lerngns3/main/scripts/openvpn.sh | bash -
    sudo systemctl restart openvpn
    
Die Client Konfigurationsdatei wird unter `~/data/.ssh/<hostname>.ovpn` erstellt.

Die Client Konfigurationsdatei auf den Notebook kopieren und `remote` IP-Adresse und Port ggf. anpassen.

    <connection>
    remote 10.9.39.8 1194 tcp4
    </connection>

wird zu:

    <connection>
    remote cloud.xxx.yy 11xxx tcp4
    </connection>

Port siehe unten bei Port Weiterleitung.

### Variante c) Ubuntu Server

Für Ubuntu Server muss eine Bridge eingerichtet werden.

Dazu die Bridge Utils installieren und die Netplan Netzwerk Konfiguration ändern

    sudo apt-get install -y bridge-utils net-tools
    export ETH=$(ip link | awk -F: '$0 !~ "lo|vir|wl|tap|br|wg|docker0|^[^0-9]"{print $2;getline}')
    export ETH=$(echo $ETH | sed 's/ *$//g')
    
    cat <<EOF | sudo tee /etc/netplan/50-cloud-init.yaml
    network:
        version: 2
        ethernets:
            ${ETH}:
                dhcp4: false
                dhcp6: false
        bridges:
          br0:
           dhcp4: true
           interfaces:
             - ${ETH}
    EOF
    
    # Ubuntu 22.x hat sich MAC Adresse verhalten geaendert
    sudo sed -i -e 's/MACAddressPolicy=persistent/MACAddressPolicy=none/g' /usr/lib/systemd/network/99-default.link
    
    sudo netplan generate
    sudo netplan apply
    
Zum Schluss OpenVPN installieren

    curl -sfL https://raw.githubusercontent.com/mc-b/lerngns3/main/scripts/openvpn.sh | bash -
    sudo systemctl restart openvpn  
    
### Port Weiterleitung

Wenn der Server mit OpenVPN nicht via Internet zugänglich ist, kann der Port mittels SSH-Tunnel an ein Server weitergeleitet werden, wo im Internet steht.

Die Syntax ist wie folgt:

    ssh -i ~/.ssh/ssh_tunnel -N -R <Port auf Gateway>:localhost:1194 <Gateway Server>
    
Um den SSH Tunnel bei Starten des System automatisch mit zu starten, ist dieser als Systemd Service einzurichten.

Dazu zuerst die Konfigurationsdatei erstellen und dann den Service aktivieren und starten.

    cat <<EOF | sudo tee /etc/systemd/system/ssh-tunnel.service
    [Unit]
    Description=SSH Tunnel for OpenVPN
    After=network-online.target
    
    [Service]
    Type=simple
    WorkingDirectory=/home/ubuntu
    User=ubuntu
    Group=ubuntu
    ExecStart=ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeychecking=no -i .ssh/ssh_tunnel -N -R <Port auf Gateway>:localhost:1194 <Gateway Server>
    ExecReload=/bin/kill -HUP $MAINPID
    KillMode=process
    Restart=on-failure
    
    [Install]
    WantedBy=multi-user.target   
    EOF

Anpassen der Einträge <Port auf Gateway> und <Gateway Server> in der oben erstellten Datei, z.B. mit vi, nano etc. 
    
    sudo -i
    systemctl daemon-reload
    systemctl enable ssh-tunnel.service
    systemctl start ssh-tunnel.service
    
**ACHTUNG** nicht vergessen den SSH-Key `ssh_tunnel` unter `/home/ubuntu/.ssh/ssh_tunnel` abzulegen. Dieser Key kann von der Lehrpersonen bezogen werden.    

* [ssh-tunnel](https://github.com/mc-b/lernmaas/blob/master/doc/MAAS/GatewayClient.md#ssh-tunnel) 
