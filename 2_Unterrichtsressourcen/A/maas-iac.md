## MAAS VM anlegen

[[_TOC_]]

### Maschinen Life Cycle

Jede von MAAS verwaltete Machine durchläuft einen Lebenszyklus (Stati):
* Registrierung (New), hardwarespezifische Elemente prüfen (Commissioning), Inventarisierung und Einrichtung von Firmware (Ready) 
* Bereitstellung (Deploy) 
um sie schliesslich zurück (Release) in den Ruhestand (Ready) zu entlassen.

**Links**
* [About machines](https://maas.io/docs/about-the-machine-life-cycle)