## VM mit Services in der Cloud anlegen

[[_TOC_]]

Verwendet jeweils eines der nachfolgenden Cloud-init Scripte um eine Virtuelle Maschine (VM), inkl. Services, zu installieren.

![](../x_gitressourcen/cloud-init.png)

- - -

Dazu ist jeweils eines der Scripte, beim erstellen der VM, in das Feld `user-data` abzufüllen.

***
### nginx Web Server

![](../x_gitressourcen/nginx.png)


**Ubuntu Linux**

nginx ist eine von Igor Sysoev entwickelte, unter der BSD-Lizenz veröffentlichte Webserver-Software, Reverse Proxy und E-Mail-Proxy.

    #cloud-config - Installiert den nginx Web Server
    packages:
     - nginx
     - shellinabox
     
Überprüft das Ergebnis, durch Anwählen der IP-Adresse Eurer VM im Browser.     

***
### Apache Web Server mit einer Intro Seite

![](../x_gitressourcen/intro.png)


    #cloud-config - Erstellt eine Intro Seite und installiert den Apache Web Server
    packages:
     - git
     - shellinabox     
    runcmd:
     - git clone https://github.com/mc-b/lernmaas /home/ubuntu/lernmaas
     - git clone https://github.com/mc-b/virtar /home/ubuntu/virtar
     - cd /home/ubuntu/virtar
     - sudo bash -x /home/ubuntu/lernmaas/helper/intro
     - sudo cp -rp images /var/www/html/ 

Überprüft das Ergebnis, durch Anwählen der IP-Adresse Eurer VM im Browser.

**Tipp**

Die Installation des Paketes `shellinabox` ermöglicht es, sich mittels `https://<ip vm>:4200` mit der VM zu verbinden und so auf die Shelloberfläche zu gelangen.

**Links**

* [Offizielle Cloud-init Beispiele](https://cloudinit.readthedocs.io/en/latest/topics/examples.html)
* [lernMAAS und Cloud-init in der Public Cloud](https://github.com/mc-b/lernmaas/tree/master/doc/Cloud)
