## VM mit Services, mittels CLI, anlegen

[[_TOC_]]

Erstellt mit NotePad, vi, nano etc. eine Datei mit Namen cloud-init.yaml.

**Als Inhalt verwendet Ihr ein Cloud-init Beispiel aus** [VM mit Services in der Cloud anlegen](cloud-iac.md).

Mittels dieser Datei und dem jeweiligen Cloud CLI, erstellen wir eine neu VM.

### Azure Cloud

Anmelden an der Azure Cloud 

    az login
 
Anschliessend müssen folgende Aktionen ausgeführt werden:
* Erstellen einer Ressource Gruppe, wo unsere VMs abgelegt werden:    
* Erstellen der VM 
* Freigeben des Ports 80, damit wir via Browser auf die VM bzw. die Installierten Services zugreifen können.

<pre>
az group create --name mygroup --location switzerlandnorth
az vm create --resource-group mygroup --name myvm --image UbuntuLTS --size Standard_D2_v4 --location switzerlandnorth --custom-data cloud-init.yaml --generate-ssh-keys --public-ip-sku Standard
az vm open-port --port 80 --resource-group mygroup --name myvm
</pre>    
    
**Überprüft das Ergebnis, durch Anwählen der IP-Adresse Eurer VM im Browser.**

Um die VM zu löschen, genügt es, die Resource Gruppe zu löschen.    

    az group delete --name mygroup --yes 
    
[Schritt für Schritt Anleitung](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-cli)       
    
### AWS Cloud

Einloggen in AWS Cloud

    aws configure
 
    AWS Access Key ID [****************WBM7]:
    AWS Secret Access Key [****************eKJA]:
    Default region name [us-east-1]:
    Default output format [None]:
    
Anschliessend müssen folgende Aktionen ausgeführt werden:
* Security Group erstellen und Ports öffnen
* Erstellen der VM 

<pre>
aws ec2 create-security-group --group-name mygroup --description "Standard Ports"
aws ec2 authorize-security-group-ingress --group-name mygroup --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name mygroup --protocol tcp --port 80 --cidr 0.0.0.0/0   
    
aws ec2 run-instances --image-id ami-0767046d1677be5a0 --security-group-ids mygroup --instance-type t2.micro --count 1 --user-data file://cloud-init.yaml 
</pre>

Anschliessend können wir uns die laufenden VMs anzeigen

    aws ec2 describe-instances --output table    
    
**Überprüft das Ergebnis, durch Anwählen der IP-Adresse Eurer VM im Browser.**

[Schritt für Schritt Anleitung](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2.html)

### MAAS (Private Cloud)

**VMs erstellen**

Dazu ist zuerst die IDs der KVM Maschinen (alt Pod) holen:

    maas ${PROFILE} pods read | jq '.[] | { name: .name, id: .id}'
    
Eine ID aussuchen und VM erstellen

    maas ${PROFILE} pod compose ${pod} hostname=test
    
Die `system_id` notieren, die brauchen wir für die Bereitstellung (Deploy) der Software auf der VM.

    maas ${PROFILE} machine deploy ${system_id}
    
Steht ein Cloud-init Script zur Verfügung, kann dieses im Base64 Format (auf einer Zeile) mitgegeben werden.      

    maas ${PROFILE} machine deploy ${system_id} data=$(base64 -w0 cloud-init.yaml)
    
Wird die VM nicht mehr gebraucht, zuerst `release` dann `delete` ausführen.

    maas ${PROFILE} machine release ${system_id}
    maas ${PROFILE} machine delete ${system_id}
    
**Hinweis**

`$PROFILE` ist der Username innerhalb von MAAS. Diese wurde vorgängig als Umgebungvariable gesetzt.    

### Multipass (Lokal NB)

Multipass ist ein Tool zum schnellen Generieren von Ubuntu-VMs im Cloud-Stil unter Linux, macOS und Windows.

**VMs erstellen**

    multipass launch --name nginx --cloud-init cloud-init.yaml
    
In die VM wechseln 

    multipass shell ngnix
    
Wenn die VM nicht mehr gebraucht, kann diese wieder gelöscht werden

    multipass delete nginx --purge
    
**Tipp**

Auf Windows wird die VM in Hyper-V erstellt und die NGINX Oberfläche mittels [http://nginx.mshome.net](http://nginx.mshome.net) erreichbar. 

  
### Links

* [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/)
* [AWS CLI](https://aws.amazon.com/de/cli/)
* [Offizielle Cloud-init Beispiele](https://cloudinit.readthedocs.io/en/latest/topics/examples.html)
* [lernMAAS und Cloud-init in der Public Cloud](https://github.com/mc-b/lernmaas/tree/master/doc/Cloud)
