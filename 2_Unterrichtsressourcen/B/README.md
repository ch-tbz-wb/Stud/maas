## Der Weg in die Cloud 

[[_TOC_]]


# Erfolgreiche Cloud-Migration: Strategie statt überstürztem Lift-and-Shift

## Die Notwendigkeit einer strukturierten Cloud-Migration

Die Verlagerung von Anwendungen in die Cloud ist nicht mehr eine Frage des „Ob“, sondern des „Wann“ und „Wie“. Unternehmen profitieren von der Modernisierung ihrer IT-Landschaft, indem sie veraltete oder nicht mehr benötigte Eigenentwicklungen eliminieren und gleichzeitig den Weg für eine digitale Transformation ebnen. Doch eine unüberlegte Migration kann zu unerwarteten Herausforderungen führen – insbesondere bei einem reinen **Lift-and-Shift-Ansatz**, der oft nur begrenzte Vorteile bringt.

## Ganzheitliche Planung als Erfolgsfaktor

Eine erfolgreiche Cloud-Migration erfordert eine umfassende Strategie, die alle relevanten Bereiche berücksichtigt: 

- **Anwendungen** und deren Architektur  
- **IT-Infrastruktur** und Betriebsmodelle  
- **Datenverwaltung** und Compliance-Anforderungen  
- **IT-Security, Governance und Datenschutz**  

Ebenso wichtig ist die Wahl des passenden Cloud-Betriebsmodells: Unternehmen können auf eine **Private Cloud**, die **Public Cloud eines Hyperscalers** oder eine **Hybrid- oder Multi-Cloud-Strategie** setzen. Eine gründliche Dokumentation der bestehenden IT-Umgebung ist essenziell, um eine flexible und zukunftssichere Cloud-Architektur zu entwerfen.

## Strategische Defizite und unklare Zielsetzungen

In vielen Unternehmen mangelt es noch an klaren Cloud-Strategien. Häufig wird der Wechsel in die Cloud überstürzt und ohne eine tiefgehende Analyse der langfristigen Ziele durchgeführt. Eine Cloud-Strategie sollte jedoch stets in die übergeordnete **IT- und Geschäftsstrategie** eingebettet sein.  

Entscheidend ist die **präzise Definition der Migrationsziele**, die von Kosteneinsparungen über eine höhere geschäftliche Agilität bis hin zu einer schnelleren Markteinführung neuer Produkte reichen können. Unklare oder zu vage formulierte Zielsetzungen erschweren jedoch eine zielgerichtete Umsetzung und führen nicht selten zu ineffizienten Cloud-Nutzungen.

## Vorteile der Cloud: Skalierbarkeit, Effizienz und Sicherheit

Einigkeit besteht darüber, dass die Cloud erhebliche Vorteile gegenüber einer klassischen **On-Premises-Infrastruktur** bietet. Besonders hervorzuheben sind:

- **Dynamische Skalierbarkeit**: Rechenleistung kann flexibel angepasst werden, um Lastspitzen effizient abzufangen.  
- **Geringerer Wartungsaufwand**: Cloud-Anbieter übernehmen Investitionen in moderne Hardware, Security-Maßnahmen und Compliance-Anforderungen.  
- **Energieeffizienz**: Hyperscaler betreiben ihre Rechenzentren oft nachhaltiger als unternehmenseigene Data Center.  

On-Premises-Infrastrukturen erfordern dagegen hohe Investitionen in Hardware und IT-Personal, um mit wachsenden Datenmengen und steigenden Sicherheitsanforderungen Schritt zu halten.

## Migrationsstrategien im Vergleich: Lift & Shift vs. Lift & Reshape

### **Lift & Shift: Der schnelle, aber eingeschränkte Ansatz**  
Beim **Lift & Shift** werden Anwendungen und Daten nahezu unverändert in die Cloud verschoben, meist im Rahmen eines **Infrastructure-as-a-Service (IaaS)**-Modells. Dies ermöglicht eine schnelle Migration mit minimalem Aufwand.  

❌ **Nachteile**:
- Cloud-spezifische Vorteile wie **automatische Skalierung, Lastverteilung oder Statusüberwachung** werden nicht genutzt.  
- Die Betriebskosten können durch ineffiziente Ressourcennutzung steigen.  

### **Lift & Reshape: Optimierung für eine zukunftsfähige Cloud-Nutzung**  
Ein weiterentwickelter Ansatz ist **Lift & Reshape**, bei dem während der Migration **Optimierungen an der Infrastruktur und Software** vorgenommen werden. Dazu gehören:  
- Anpassung der **virtuellen Maschinen** (z. B. Reduzierung der CPU-Kerne zur Kosteneinsparung).  
- **Aktualisierung des Betriebssystems** oder Modernisierung der Anwendung.  
- **Migration von Datenbanken zu PaaS-Lösungen**, inklusive Wechsel der Datenbank-Engine (z. B. von Oracle zu PostgreSQL zur Lizenzkostenreduktion).  

✅ **Vorteile**:
- Nutzung von Cloud-nativen Funktionen wie **Lastverteilung und Skalierbarkeit**.  
- Geringerer Verwaltungsaufwand für Infrastruktur durch **automatisierte Prozesse**.  

## Fazit: Cloud-Migration mit Bedacht angehen

Die Cloud bietet zahlreiche Chancen, doch ihr volles Potenzial lässt sich nur mit einer durchdachten Strategie ausschöpfen. Unternehmen sollten nicht auf eine **schnelle Lift-and-Shift-Migration** setzen, sondern gezielt **Optimierungen vornehmen**, um langfristig von einer flexiblen, skalierbaren und kosteneffizienten Cloud-Umgebung zu profitieren.


### Hands-on

**Hyper-V nach KVM** (optional)
* [Converting a Hyper-V vhdx for use with KVM or Proxmox VE](https://www.servethehome.com/converting-a-hyper-v-vhdx-for-use-with-kvm-or-proxmox-ve/)
* [Aufbereiten für MAAS.io](https://github.com/mc-b/terra/tree/main/01-5-packer-windows#aufbereiten-f%C3%BCr-maasio)

**Migration in die AWS-Cloud**

* [Migrieren Ihrer On-Premise-Workloads zu AWS](https://aws.amazon.com/de/free/migration/)

**Migration in die Azure-Cloud**

* [Azure-Migration und -Modernisierung mit einer einheitlichen Plattform vereinfachen.](https://azure.microsoft.com/de-de/products/azure-migrate)

**Migration mittels Packer** (nur Fortgeschrittene)

* [AWS Import](https://developer.hashicorp.com/packer/integrations/hashicorp/amazon/latest/components/post-processor/import)
* [How to use Packer to create Linux virtual machine images in Azure](https://learn.microsoft.com/en-us/azure/virtual-machines/linux/build-image-with-packer)
* [MAAS.io](https://github.com/canonical/packer-maas)
* [Windows Beispiele aus dem Terraform Kurs](https://github.com/mc-b/terra/tree/main/01-5-packer-windows)


### Selbststudium

**Allgemeine Einführung**

* [Wege in die Cloud: Die 6 R’s der Cloud-Migration](https://blog.materna.de/cloud-migration/)

**Packer Import**

* [Azure Cloud](https://learn.microsoft.com/en-us/azure/virtual-machines/linux/build-image-with-packer )
* [AWS Cloud](https://developer.hashicorp.com/packer/integrations/hashicorp/amazon/latest/components/post-processor/import)


 