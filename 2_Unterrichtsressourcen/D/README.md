Multi-Cloud
===========

Grundlagen
----------

### Multi-Cloud


Eine Multi-Cloud ist von Natur aus kein "einzelnes Ding", sondern eine Reihe von unabhängigen Dingen, die unter eine zentrale Verwaltung gestellt werden.

Für die  zentrale Verwaltung verwenden wir den Rack-Server.

Beispielapplikation
-------------------

![](https://github.com/mc-b/duk/raw/e85d53e7765f16833ccfc24672ae044c90cd26c1/data/jupyter/demo/images/Microservices-REST.png)

Quelle: Buch Microservices Rezepte
- - -

Die Beispielapplikation basiert auf den Cloud-init Scripts aus Kapitel [E](../E#beispielapplikation) mit einem vorangestelltem [Reverse Proxy](../E#reverse-proxy).

Neu verteilen wir aber die VMs auf drei verschiedene Clouds und lösen die Installation via [GitLab CI/CD](https://docs.gitlab.com/ee/ci/), sprich wir wenden GitOps an.

Die VMs werden dabei wie folgt verteilt:
* customer - interne Cloud (MAAS.io), z.B. wegen Sicherheitsüberlegungen (Kreditkarteninformationen etc.).
* catalog - AWS Cloud, z.B. weil wir auf Produktdaten von Lieferanten zugreifen wollen, welche ebenfalls auf der AWS Cloud gespeichert sind.
* order und Reverse Proxy - Azure Cloud.

Alle VMs können können untereinander mittels eines WireGuard VPNs kommunizieren.

Installation
------------

Als zentrale Verwaltung verwenden wir den Rack-Server. Dazu brauchen wir die CLI Tools der verschiedenen Cloud Anbieter.

Anmelden auf dem Rack-Server und CLI der Cloud Anbieter installieren:

    ssh ubuntu@<IP Rack-Server>

    # Azure CLI
    curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
    # AWS CLI
    sudo apt-get install awscli

Für das MAAS CLI siehe [MAAS CLI und Tests](../C/maas-install.md#maas-cli-und-tests).

### WireGuard

Als erstes erstellen wir die WireGuard Keys und legen diese, für später, in der Datei `keys.wg` ab.

    rm -f keys.wg
    for vm in order customer catalog rp
    do
        HOST_KEY=$(wg genkey)
        echo -e "${vm}_key\t${HOST_KEY}" >>keys.wg
        echo -e "${vm}_pub\t$(echo ${HOST_KEY} | wg pubkey)" >>keys.wg
    done        
   

### Installation Reverse Proxy (Azure Cloud)

Der Reverse Proxy ist zugleich unser WireGuard Server, darum legen wir diesen zuerst an.

Zuerst brauchen wir das Cloud-init Script mit den WireGuard Keys

    cat <<EOF >cloud-init-rp.yaml
    #cloud-config
    packages:
      - apache2
    write_files:
     - content: |
        <html>
         <body>
          <h1>My Application</h1>
           <ul>
           <li><a href="/order">Order</a></li>
           <li><a href="/customer">Customer</a></li>
           <li><a href="/catalog">Catalog</a></li>
           </ul>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'  
     - content: |
        ProxyRequests Off
        <Proxy *>
              Order deny,allow
              Allow from all
        </Proxy>
        ProxyPass /order http://192.168.100.101       
        ProxyPassReverse /order http://192.168.100.101
        ProxyPass /catalog http://192.168.100.102       
        ProxyPassReverse /catalog http://192.168.100.102
        ProxyPass /customer http://192.168.100.103
        ProxyPassReverse /customer http://192.168.100.103
       path: /etc/apache2/sites-enabled/001-reverseproxy.conf
       permissions: '0644'  
    wireguard:
      interfaces:
        - name: wg0
          config_path: /etc/wireguard/wg0.conf
          content: |
            [Interface]
            Address = 192.168.100.1
            ListenPort = 51820
            PrivateKey = $(cat keys.wg | grep rp_key | cut -f2)
            
            ### Order  $(cat keys.wg | grep order_key | cut -f2)
            [Peer]
            PublicKey = $(cat keys.wg | grep order_pub | cut -f2)
            AllowedIPs = 192.168.100.101
            
            ### Customer  $(cat keys.wg | grep customer_key | cut -f2)
            [Peer]
            PublicKey = $(cat keys.wg | grep customer_pub | cut -f2)
            AllowedIPs = 192.168.100.102
            
            ### Catalog  $(cat keys.wg | grep catalog_key | cut -f2)
            [Peer]
            PublicKey = $(cat keys.wg | grep catalog_pub | cut -f2)
            AllowedIPs = 192.168.100.103
    runcmd:
     - sudo a2enmod proxy
     - sudo a2enmod proxy_html
     - sudo a2enmod proxy_http
     - sudo service apache2 restart     
    EOF

Dann folgt die VM. Als erstes müssen wir uns in der Azure Cloud anmelden 

    az login
 
Anschliessend müssen folgende Aktionen ausgeführt werden:
* Erstellen einer Ressource Gruppe, wo unsere VMs abgelegt werden:    
* Erstellen der VM 
* Freigeben des Ports 80 und 51820, damit wir Zugriff auf den Reverse Proxy und WireGuard Server haben.

Die Befehle sind wie folgt
    
    az group create --name webshop --location southcentralus
    az vm create --resource-group webshop --name rp --image Ubuntu2204 --size Standard_B1s --location southcentralus \
                 --custom-data cloud-init-rp.yaml --public-ip-sku Standard
    az vm open-port --port 80 --resource-group webshop --name rp --priority 1010
    az vm open-port --port 51820 --resource-group webshop --name rp --priority 1020
    
Wenn es nicht funktioniert hat, genügt es die Resource Gruppe zu löschen

    az group delete --name webshop --yes    
    
Um weiterzuarbeiten brauchen wir die IP-Adresse der erstellen VM

    export RPIP=$(az vm list --resource-group webshop -d --query '[0].publicIps' | tr -d '"')

### Installation Order (Azure Cloud)

Wie vorher, zuerst das Cloud-init Script mit den WireGuard Keys erstellen:

    cat <<EOF >cloud-init-order.yaml
    #cloud-config
    packages:
      - nginx
    wireguard:
      interfaces:
        - name: wg0
          config_path: /etc/wireguard/wg0.conf
          content: |
            [Interface]
            PrivateKey = $(cat keys.wg | grep order_key | cut -f2)
            Address = 192.168.100.101
            [Peer]
            PublicKey = $(cat keys.wg | grep rp_pub | cut -f2)
            Endpoint = ${RPIP}:51820
            AllowedIPs = 192.168.100.0/24
            PersistentKeepalive = 25
    write_files:
     - content: |
        <html>
         <body>
          <h1>Order App</h1>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'
    EOF
    
Dann die VM    
    
    az vm create --resource-group webshop --name order --image Ubuntu2204 --size Standard_B1s --location southcentralus \
                 --custom-data cloud-init-order.yaml --public-ip-sku Standard
        
### Installation Catalog (AWS Cloud)

Zuerst das Cloud-init Script mit den WireGuard Keys erstellen:

    cat <<EOF >cloud-init-catalog.yaml
    #cloud-config
    packages:
      - nginx
    wireguard:
      interfaces:
        - name: wg0
          config_path: /etc/wireguard/wg0.conf
          content: |
            [Interface]
            PrivateKey = $(cat keys.wg | grep catalog_key | cut -f2)
            Address = 192.168.100.102
            [Peer]
            PublicKey = $(cat keys.wg | grep rp_pub | cut -f2)
            Endpoint = ${RPIP}:51820
            AllowedIPs = 192.168.100.0/24
            PersistentKeepalive = 25
    write_files:
     - content: |
        <html>
         <body>
          <h1>Catalog App</h1>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'
    EOF
    

Einloggen in AWS Cloud

    aws configure
 
    AWS Access Key ID [****************WBM7]:
    AWS Secret Access Key [****************eKJA]:
    Default region name [us-east-1]:
    Default output format [None]:
    
Anschliessend müssen folgende Aktionen ausgeführt werden:
* Security Group erstellen und Ports öffnen
* Erstellen der VM 

Die Befehle sind wie folgt:

    aws ec2 create-security-group --group-name catalog --description "Standard Ports"
    aws ec2 authorize-security-group-ingress --group-name catalog --protocol tcp --port 22 --cidr 0.0.0.0/0
    
    aws ec2 run-instances --image-id ami-053b0d53c279acc90 --security-group-ids catalog --instance-type t2.micro --count 1 --user-data file://cloud-init-catalog.yaml


Anschliessend können wir uns die laufenden VMs anzeigen

    aws ec2 describe-instances --output table   
    
### Installation Customer (MAAS.io - Private Cloud)

Zuerst das Cloud-init Script mit den WireGuard Keys erstellen:

    cat <<EOF >cloud-init-customer.yaml
    #cloud-config
    packages:
      - nginx
    wireguard:
      interfaces:
        - name: wg0
          config_path: /etc/wireguard/wg0.conf
          content: |
            [Interface]
            PrivateKey = $(cat keys.wg | grep customer_key | cut -f2)
            Address = 192.168.100.103
            [Peer]
            PublicKey = $(cat keys.wg | grep rp_pub | cut -f2)
            Endpoint = ${RPIP}:51820
            AllowedIPs = 192.168.100.0/24
            PersistentKeepalive = 25
    write_files:
     - content: |
        <html>
         <body>
          <h1>customer App</h1>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'
    EOF

Einlogen für CLI Access (die Umgebungsvariable wurde bei der Installation in `~/.bashrc` abgelegt)

    maas login ${PROFILE} http://localhost:5240/MAAS/api/2.0
    
Der verlangte API Key finden wir im UI von MAAS unter `ubuntu` -> `API-Keys`.    

Dazu ist zuerst die IDs der KVM Maschinen (alt Pod) holen:

    maas ${PROFILE} pods read | jq '.[] | .id'
    
Eine ID (Pod/KVM-Maschine) aussuchen und VM erstellen

    maas ${PROFILE} pod compose ${pod} hostname=test
    
Die `system_id` notieren, die brauchen wir für die Bereitstellung (Deploy) der Software auf der VM.

Zum Schluss Erstellen wir die VM. Das Cloud-init Script, kann im Base64 Format (auf einer Zeile) mitgegeben werden.      

    maas ${PROFILE} machine deploy ${system_id} data=$(base64 -w0 cloud-init-customer.yaml)    


### Links

* [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/)
* [AWS CLI](https://aws.amazon.com/de/cli/)
* [Offizielle Cloud-init Beispiele](https://cloudinit.readthedocs.io/en/latest/topics/examples.html)
* [lernMAAS und Cloud-init in der Public Cloud](https://github.com/mc-b/lernmaas/tree/master/doc/Cloud)    
