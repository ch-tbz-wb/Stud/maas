# Handlungskompetenzen

## Kompetenzmatrix

| **Kompetenzband**                            | **RP**                                                                                 | **HZ** | **Novizenkompetenz**                                                                 | **Fortgeschrittene Kompetenz**                                                       | **Kompetenz professionellen Handelns**                                             | **Kompetenzexpertise**                                                                 |
|--------------------------------------|---------------------------------------------------------------------------------------|-------|-------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------|----------------------------------------------------------------------------------------|
| **a) Cloud - Funktionsweise, Servicemodelle** | B7.1, B7.4, B12.3, C1.2, C1.3                                                                     |   1    | Grundlegendes Verständnis der Cloud-Funktionsweise.                               | Fähigkeit, verschiedene Cloud-Servicemodelle zu erklären.                   | Analyse und Auswahl von Cloud-Servicemodellen für spezifische Anwendungsfälle. | Tiefergehendes Wissen über komplexe Cloud-Architekturen und ihre Anwendungsfälle.       |
| **b) Der Weg in die Cloud**           | A3.1, A3.3, B7.1, B7.4, C1.5, B4.6                                                                |  2    | Kenntnis der grundlegenden Schritte zur Cloud-Migration.                           | Fähigkeit, eine Migrationsstrategie zu entwickeln.                           | Bewerten von Risiken und Erstellen eines Risikomanagementplans.            | Führung und Leitung komplexer Migrationsprojekte unter Berücksichtigung globaler Best Practices. |
| **c) Private Cloud Einrichten**       | B7.1, B7.4, C1.1, C1.4, B14.1, B14.2, B12.1                                                       |   3    | Grundlegendes Verständnis der Private Cloud und ihrer Anforderungen.               | Planung und Implementierung einer Private Cloud.                            | Überwachung und Optimierung der Private Cloud.                            | Innovatives Design und kontinuierliche Verbesserung von Private Cloud-Architekturen.     |
| **d) Multi Cloud**                    | B7.1, B7.4, B12.4, C1.5, A2.1, A2.3, A2.5                                                         |   4    | Grundverständnis von Multi-Cloud-Konzepten und deren Vorteilen.                    | Fähigkeit, eine Multi-Cloud-Architektur zu entwickeln.                      | Integration und Management von Multi-Cloud-Umgebungen.                    | Expertenwissen in der Orchestrierung und Optimierung von Multi-Cloud-Umgebungen.         |

* RP = Rahmenlehrplan
* HZ = Handlungsziele

---

## Modulspezifische Handlungskompetenzen

 * B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
   * B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)
   
 * B7 Technische Anforderungen analysieren und bestimmen
   * B7.1 Die ICT-Architektur zielorientiert (ICT Strategie) analysieren, beurteilen und bestimmen (Niveau: 4)
   * B7.4 Den Einsatz von geschäftsrelevanten Systemen konzipieren (Niveau: 3)

 * B12 System- und Netzwerkarchitektur bestimmen
   * B12.1 Die bestehende Systemarchitektur beurteilen und weiterentwickeln (Niveau: 3)
   * B12.3 Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln (Niveau: 3)
   * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen (Niveau: 3)   

 * B14 Konzepte und Services umsetzen
   * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten (Niveau: 3)
   * B14.2 Probleme und Fehler im operativen Betrieb überwachen, identifizieren, zuordnen, beheben oder falls erforderlich eskalieren (Niveau: 3)

* C1 Cloud
  * C1.1 Analysiert die bestehende ICT-Umgebung und deren Struktur und beurteilt die Möglichkeit eine Private Cloud einzusetzen (Niveau: 2)
  * C1.2.  Kann erste Entscheidungshilfen für sein Unternehmen liefern welche Cloud Services sinnvoll sind (Niveau: 3)
  * C1.3.  Der Studierende ist in der Lage, Systeme zu evaluieren, deren Performance und Eignung für den Cloud Anwendungszweck zu beurteilen (Niveau: 3)
  * C1.4.  Der Studierende ist in der Lage ein Private Cloud System aufzusetzen und zu betreiben (Niveau: 2)
  * C1.5.  Der Studierende ist in der Lage Services für die Cloud bereitzustellen, diese zu automatisieren und jederzeit zur reproduzieren (Niveau: 4)   

## Allgemeine Handlungskompetenzen

 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren

 * A3 Die persönliche Entwicklung reflektieren und aktiv gestalten
   * A3.1 Die eigenen Kompetenzen bezüglich der beruflichen Anforderungen regelmässig reflektieren, bewerten und daraus den Lernbedarf ermitteln
   * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren
   * A3.3 Neue Technologien kritisch reflexiv beurteilen, adaptieren und integrieren
   * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln
   * A3.5 Das eigene Denken, Fühlen und Handeln reflektieren und geeignete persönliche Entwicklungsmassnahmen definieren und umsetzen   

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
