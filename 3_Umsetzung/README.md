# Umsetzung

- Bereich: Konzepte und Services umsetzen
- Semester: 1 

## Lektionen

* Präsenzlektionen (Vor Ort): 30
* Präsenzlektionen (Distance Learning): 10
* Selbststudium: 20

### Fahrplan

| **Lektionen** | **Selbststudium** | **Inhalt**                            | **Kompetenzenband**                                                                 | **Tools**                                   |
|---------------------------------------------------|------------------|--------------------------------------|-------------------------------------------------------------------------------------|---------------------------------------------|
| 11     | 5                | Cloud - Funktionsweise, Servicemodelle | a)                                                                   | Präsentationen, Whiteboard, Online-Videos  |
| 10   | 4                | Der Weg in die Cloud                 | b)                                                             | Cloud-Simulations-Tools, Fallstudien       |
| 10   | 5                | Private Cloud Einrichten             | c)                                                   | Virtualisierungssoftware, Dokumentationen  |
| 9   | 6                | Multi Cloud                         | d)                                                       | Cloud-Management-Plattformen, Fallstudien  |
| **Total 40**  | **Total 20**  |   **-**                |    **-**                        |      **-**                              |

## Voraussetzungen

* [Linux Essentials - Die Einsteiger-Zertifizierung des LPI](https://learning.lpi.org/de/learning-materials/010-160/)
* [Linux-Administration II Linux im Netz](https://www.tuxcademy.org/product/adm2/), Kapitel 10 - Die Secure Shell
* [Debian-Paketverwaltung verwenden](https://learning.lpi.org/de/learning-materials/101-500/102/102.4/) 
* Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren und zwei Netzwerk verbinden. Module [117](https://gitlab.com/ch-tbz-it/Stud/m117/) und [145](https://gitlab.com/alptbz/m145/)

## Dispensation 

keine

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

MAAS - Meta as a Service, IaaS - Infrastructure as a Service, PaaS - Platform as a Service, SaaS - Software as a Service


## Lerninhalte

* Cloud - Funktionsweise, Servicemodelle
* Der Weg in die Cloud
* Private Cloud Einrichten
* Multi Cloud

## Übungen und Praxis

* VM aussetzen in der Private, Public Cloud über UI und CLI
* Services Strukturieren 
* Private Cloud aufsetzen
* Multi Cloud Lösung aufsetzen

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel:  

* [MAAS.io](https://maas.io/)
* [lernMAAS](https://github.com/mc-b/lernmaas)
* [lernCloud](https://github.com/mc-b/lerncloud)
* [lernGNS3](https://github.com/mc-b/lerngns3)





