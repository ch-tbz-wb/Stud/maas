# Handlungsziele und Handlungssituationen 

### 1. Cloud - Funktionsweise, Servicemodelle

- Verstehen der grundlegenden Funktionsweise von Cloud-Computing.
- Erklären der verschiedenen Cloud-Servicemodelle (IaaS, PaaS, SaaS) und deren Unterschiede.
- Analysieren der Vor- und Nachteile der verschiedenen Cloud-Servicemodelle für spezifische Anwendungsfälle.

**Typische Handlungssituation**:
Du arbeitest in einem mittelständischen Unternehmen, das bisher alle IT-Systeme On-Premise betrieben hat. Der Geschäftsführer hat sich über die Vorteile von Cloud-Computing informiert und bittet dich, eine Präsentation für das Management zu erstellen. In dieser Präsentation sollst du die grundlegende Funktionsweise von Cloud-Computing erklären, die verschiedenen Cloud-Servicemodelle (IaaS, PaaS, SaaS) vorstellen und deren Unterschiede erläutern. Ausserdem möchte das Management eine Analyse der Vor- und Nachteile dieser Servicemodelle in Bezug auf ihre speziellen IT-Anforderungen.


### 2. Der Weg in die Cloud

- Erkennen der wichtigsten Schritte und Überlegungen beim Übergang in die Cloud.
- Entwickeln einer Migrationsstrategie für den Übergang von On-Premise-Systemen in die Cloud.
- Bewerten von Risiken und Herausforderungen bei der Cloud-Migration und Erstellen eines Risikomanagementplans.

**Typische Handlungssituation**:
Dein Unternehmen hat sich entschieden, einige der IT-Systeme in die Cloud zu migrieren, um mehr Flexibilität und Skalierbarkeit zu erreichen. Deine Aufgabe ist es, die ersten Schritte der Cloud-Migration zu planen. Du musst die wichtigsten technischen und organisatorischen Aspekte beim Übergang in die Cloud identifizieren, eine Migrationsstrategie entwickeln und die Risiken analysieren, die bei einer Cloud-Migration auftreten können. Dabei sollst du auch einen Risikomanagementplan entwerfen, um mögliche Herausforderungen frühzeitig zu adressieren.

### 3. Private Cloud Einrichten

- Verstehen der Prinzipien und Anforderungen für die Einrichtung einer Private Cloud.
- Planen und Implementieren einer Private Cloud-Infrastruktur unter Berücksichtigung von Sicherheit und Compliance.
- Überwachen und Optimieren einer Private Cloud-Infrastruktur für Effizienz und Leistung.

**Typische Handlungssituation**:
Ein Finanzdienstleister, für den du arbeitest, benötigt aufgrund von Datenschutzanforderungen eine Private Cloud-Lösung. Deine Aufgabe ist es, die bestehenden Systeme zu analysieren und die Anforderungen für die Einrichtung einer Private Cloud zu bestimmen. Danach planst und implementierst du die Private Cloud-Infrastruktur unter Berücksichtigung von Sicherheit und Compliance-Vorgaben. Nach der erfolgreichen Einrichtung überwachst du die Performance und optimierst die Infrastruktur, um maximale Effizienz und Sicherheit zu gewährleisten.

### 4. Multi Cloud

- Verstehen der Konzepte und Vorteile der Multi-Cloud-Strategie.
- Entwickeln einer Multi-Cloud-Architektur und Auswahl geeigneter Anbieter und Dienste.
- Integrieren und Managen von Multi-Cloud-Umgebungen unter Berücksichtigung von Interoperabilität und Kostenoptimierung.

**Typische Handlungssituation**:
Dein Unternehmen hat bereits verschiedene Cloud-Dienste bei unterschiedlichen Anbietern implementiert, möchte aber nun eine Multi-Cloud-Strategie verfolgen, um Abhängigkeiten zu vermeiden und Flexibilität zu erhöhen. Deine Aufgabe ist es, eine Multi-Cloud-Architektur zu entwerfen und die geeigneten Anbieter und Dienste auszuwählen. Danach implementierst du die Multi-Cloud-Umgebung und sorgst dafür, dass die verschiedenen Cloud-Dienste interoperabel arbeiten. Du optimierst zudem die Kostenstruktur der Multi-Cloud-Nutzung und sicherst die Performance der Dienste ab.


