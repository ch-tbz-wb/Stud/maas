![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# MAAS - Private Cloud (MAAS.io)

## Kurzbeschreibung des Moduls 

In der Public Cloud stehen eine Vielzahl von Services zur Verfügung.

* Doch was steckt hinter diesen Services?
* Wie sind diese Entstanden und wie können wir selbst solche Services entwickeln?
* Ist es sinnvoll alles in die Public Cloud zu verlagern?

Ausgehend von automatisierten, auf Self-Service-Technologien basierenden Remoteinstallationen von Betriebssystemen werden Grundlagen, Technologien und Praxis zum Bau einer Private Cloud-Lösung vermittelt. Sie lernen in diesem Modul, wie physische Rechner schnell und automatisiert auf Computerhardware installiert werden können und wie dies über diese Services effizient betrieben werden kann. Dazu werden notwendige Aktualisierungen des dazu notwendigen Vorwissens in Netzwerk und Computertechnik bearbeitet.

## Angaben zum Transfer der erworbenen Kompetenzen 

Umsetzung der erlernten Theorie in Einzel- und Gruppenarbeiten.

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

Keine.

### Nachfolgende Module

* Infrastructure as Code
* AWS
* Azure

## Dispensation

Wenn eine ähnliche Umsetzung z.B. mit [OpenStack](https://www.openstack.org/) oder [Cloud Foundry](https://www.cloudfoundry.org/) implementiert wurde.

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
